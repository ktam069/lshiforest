#! /usr/bin/python
#
# Implemented by Xuyun Zhang (email: xuyun.zhang@auckland.ac.nz). Copyright reserved.
#

import numpy as np
import pandas as pd
import csv
import time
from sklearn.metrics import roc_auc_score
from sklearn.ensemble import IsolationForest

from detectors import VSSampling
from detectors import Bagging
from detectors import LSHForest
from detectors import E2LSH, KernelLSH, AngleLSH


DISABLE_WARNINGS = True		# (may be helpful for ease of viewing - alternatively run script with -W ignore)
PRINT_DETAILS = False		# print the results of each test 	(default: False)

NUM_TESTS = 10				# number of times to test each classifier	(default: 10)

num_ensemblers = 100
rng = np.random.RandomState(42)

classifiers = [	("sklearn.ISO", IsolationForest(random_state=rng, contamination=0.1)), 
				("ALSH", LSHForest(num_ensemblers, VSSampling(num_ensemblers), AngleLSH())), 
				("L1SH", LSHForest(num_ensemblers, VSSampling(num_ensemblers), E2LSH(norm=1))), 
				("L2SH", LSHForest(num_ensemblers, VSSampling(num_ensemblers), E2LSH(norm=2))), 
				("KLSH", LSHForest(num_ensemblers, VSSampling(num_ensemblers), KernelLSH()))]

data = pd.read_csv('dat/glass.csv', header=None)
X = data.values[:, :-1].tolist()
ground_truth = data.values[:, -1].tolist()


def main():
	if DISABLE_WARNINGS:	# disable the display of warnings (which we expect there to be)
		import warnings
		warnings.filterwarnings("ignore")
	
	print "\t(Running", NUM_TESTS, "tests for each classifier)"
	for i in range(len(classifiers)-1):			# (use len(classifiers)-1 to skip the last kernel clf)
		test_classifier(i)


def test_classifier(clf_i):

	(clf_name, clf) = classifiers[clf_i]

	average_auc = 0
	average_train_time = 0
	average_test_time = 0

	print "\n\t"+clf_name+":"
	if not PRINT_DETAILS: print "\tRunning tests..."

	for i in range(NUM_TESTS):
		if PRINT_DETAILS: print "\t- Test", i, "-"
		start_time = time.time()
		clf.fit(X)
		train_time = time.time()-start_time
		y_pred = clf.decision_function(X).ravel()
		test_time = time.time()-start_time-train_time
		auc = roc_auc_score(ground_truth, -1.0*y_pred)
		if PRINT_DETAILS: print "AUC:	", auc
		if PRINT_DETAILS: print "Training time:	", train_time
		if PRINT_DETAILS: print "Testing time:	", test_time
		average_auc += auc
		average_train_time += train_time
		average_test_time += test_time
	
	average_auc /= NUM_TESTS
	average_train_time /= NUM_TESTS
	average_test_time /= NUM_TESTS
	
	if PRINT_DETAILS: print ""
	print "Average AUC:	", average_auc
	print "Average training time:	", average_train_time
	print "Average testing time:	", average_test_time


if __name__ == '__main__':
	main()
