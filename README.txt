Author: Xuyun Zhang
Email: xuyun.zhang@gmail.com
Copyright reserved

This version is for ICDE2017 submission review.
Paper title: 
	LSHiForest: A generic framework for fast tree isolation based ensemble anomaly analysis

To run the demo:
python run_demo.py or ./run_demo.py

The demon will show the results of our proposed algorithms and the seminal iForest on a real data set "glass"
