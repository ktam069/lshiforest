# Biased Sampling with LSHiForest

## Description
This repository contains the code used in the tests done in the research of the use of biased sampling on LSH Isolation Forest (LSHiForest).

## Dependencies

To run this project fully, the following libraries and tools are required.

### Language and version:

Python 2.7

### Download and install:

**Use pip install:**

* sklearn

* numpy

* pandas

* scipy

* librosa

To install all the required libraries listed above, run the following command:

```
python -m pip install -r requirements.txt
```

### Exact versions:

**If there are dependency issues, try to install these exact versions**

* Python 2.7.14

* Numpy 1.15.4

* Pandas 0.23.4

* Scikit-Learn 0.20.1

* Scipy 1.2.0

Alternatively, you can also run the code in Python 3 (use the code in the python3-version branch, or use the 2to3 library):

* Python 3.4.3:

* Numpy 1.12.1

* Pandas 0.20.1

* Scikit-learn 0.18.1

* Scipy 0.19.0


## Instructions for Running

### Dataset

The dataset used was the same as that used for "LSHiForest: A Generic Framework for Fast Tree Isolation Based Ensemble Anomaly Analysis".

### Running the Tests

Rename each dataset with just a number, e.g. "1.csv", and put them in /dat/all_data/. The file "glass.csv" can also but placed in /dat/ to use for some quick testing.

Run the following to test the models:

```python
python run_demo_looped.py
```

By default, the program will run 10 tests for each model, for each dataset, with density biased sampling enabled.

It will print all the results (accuracies and running times) when all the tests finish running.

