#!/usr/bin/python
#
# Implemented by Xuyun Zhang (email: xuyun.zhang@auckland.ac.nz). Copyright reserved.
#

import numpy as np

import lsh_node as nd

class LSHTree:
	
	def __init__(self, lsh):
		self._lsh = lsh
		self._depth_limit = 0
		self._root = None
		self._n_samples = 0 
		self._branch_factor = 0
		self._reference_path_length = 0
		

	def build(self, data):				# builds the tree, by calling _recursive_build()
		self._n_samples = len(data)
		self._depth_limit = self.get_random_height(self._n_samples)
		data = np.array(data)						# (change from list to ndarray - differences in access/speed??)
		data = self._lsh.format_for_lsh(data)		# basically data = data ? Guessing it's included for extensibility.
		self._root = self._recursive_build(data, self._depth_limit)
		self._branch_factor = self._get_avg_branch_factor()				# average v for the tree
		self._reference_path_length = self.get_random_path_length_symmetric(self._n_samples)	# normalisation factor
		

	def _recursive_build(self, data, depth_limit, lof=0, hash_func_index=0):
		n_samples = len(data)
		if n_samples == 0:
			return None
		if n_samples == 1 or hash_func_index > depth_limit:		# end condition - no further splitting needed
			return nd.LSHNode(len(data), {}, {}, hash_func_index, lof)
		else:
			cur_index = hash_func_index
			partition = self._split_data(data,cur_index)		# attempt to split data into buckets
			while len(partition) == 1 and cur_index <= depth_limit:		# repeat until v > 1 (or depth limit reached)
				cur_index += 1
				partition = self._split_data(data, cur_index)
			if cur_index > depth_limit:							# at depth limit - end recursive calls
				return nd.LSHNode(len(data), {}, {}, cur_index, lof)

			children_count = {}				# stores the size of the partitions (...what for?)
			for key in partition.keys():
				children_count[key] = len(partition.get(key))

			mean = np.mean(children_count.values()) 		# ...lof not actually used for anything?
			std = np.std(children_count.values())
				
			children = {}
			for key in partition.keys():		# for each branch, recursively build the rest of the tree
				child_data = partition.get(key)			# (key is K_i; child_data is S_i)
				children[key] = self._recursive_build(child_data, depth_limit, min(0.0, (children_count[key]-mean)/std), cur_index+1)
			return nd.LSHNode(len(data), children, children_count, cur_index, lof)


	def _split_data(self, data, depth):
		''' Split the data using LSH '''
		partition = {}				# the dictionary of {K_v : S_v}
		for i in range(len(data)):
			key = self._lsh.get_hash_value(np.array(data[i][1:]), depth)		# compute the hash for each point
			if key not in partition:				# add points to the partitions
				partition[key] = [data[i]]
			else:
				sub_data = partition[key]
				sub_data.append(data[i])
				partition[key] = sub_data
		return partition


	def get_num_instances(self):
		return self._n_samples


	def display(self):
		self._recursive_display(self._root)	

	
	def _recursive_display(self, lsh_node, leftStr=''):
		if lsh_node is None:
			return
		children = lsh_node.get_children()

		print leftStr+'('+str(len(leftStr))+','+str(lsh_node._hash_func_index)+'):'+str(lsh_node._data_size)+':'+str(lsh_node._children_count)+','+str(lsh_node._lof)
		
		for key in children.keys():
			self._recursive_display(children[key], leftStr+' ')


	def predict(self, granularity, point):			# gives the anomaly score of the point in this tree, in (0,1]
		point = self._lsh.format_for_lsh(np.mat(point)).A1
		path_length = self._recursive_get_search_depth(self._root, 0, granularity, point)	# computes h_i(x)
		return pow(2.0, (-1.0*path_length/self._reference_path_length))
			 

	def _recursive_get_search_depth(self, lsh_node, cur_depth, granularity, point):		# helper method for predict - finds h_i(x) - Alg 4
		if lsh_node is None:
			return -1
		children = lsh_node.get_children()
		if not children:
			real_depth = lsh_node._hash_func_index		# node.index = depth of uncompressed tree
			adjust_factor = self.get_random_path_length_symmetric(lsh_node.get_data_size())		# mu(node.size)
			return cur_depth*np.power(1.0*real_depth/max(cur_depth, 1.0), granularity)+adjust_factor	# note that I_cur is taken to be 1 if 0 (psi=1?)
		else:
			key = self._lsh.get_hash_value(point[1:], lsh_node.get_hash_func_index())	# hash the point
			
			if key in children.keys():													# ...and go down the corresponding branch
				return self._recursive_get_search_depth(children[key], cur_depth+1, granularity, point)
			else:								# no corresponding hash found - dissimilar instance
				cur_depth = cur_depth+1
				real_depth = lsh_node._hash_func_index+1
				return cur_depth*np.power(1.0*real_depth/max(cur_depth, 1), granularity)


	def get_avg_branch_factor(self):
		return self._branch_factor

					
	def _get_avg_branch_factor(self):			# straight average of v
		i_count, bf_count = self._recursive_sum_BF(self._root)
		# Single node PATRICIA trie
		if i_count == 0:
			return 2.0
		return bf_count*1.0/i_count


	def _recursive_sum_BF(self, lsh_node):		# helper method for _get_avg_branch_factor()
		if lsh_node is None:
			return None, None
		children = lsh_node.get_children()
		if not children:
			return 0, 0
		else:
			i_count, bf_count = 1, len(children)
			for key in children.keys():
				i_c, bf_c = self._recursive_sum_BF(children[key])
				i_count += i_c
				bf_count += bf_c
			return i_count, bf_count	

	
	def get_random_path_length_symmetric(self, num_samples):	# computes mu(sample size) - normalisation factor
		if num_samples <= 1:
			return 0
		elif num_samples > 1 and num_samples <= round(self._branch_factor):
			return 1
		else:
			return (np.log(num_samples)+np.log(self._branch_factor-1.0)+0.5772)/np.log(self._branch_factor)-0.5
	
	
	def get_random_height(self, num_samples):
		return 2*np.log2(num_samples)+0.8327
