#! /usr/bin/python
#
# Implemented by Xuyun Zhang (email: xuyun.zhang@auckland.ac.nz). Copyright reserved.
#

import numpy as np
import copy as cp

import lsh_count
import kde
import lsh_forest
import lsh

NUM_ENS = 100					# Number of ensembles used for LSHiForest in density estimation	(default: 100)

PRINT_STATS = False				# Print sample sizes, densities, and sampling probabilities		(default: False)
SAMPLE_SIZE_WARNING = False		# Show warning for too low sample size	(default: True)

class Sampling():

	def __init__(self, num_samples, bootstrap=False, bagging=None, density_biased=False, db_type="KDE", tuning_factor=0, tune_offline=False):
		self._num_samples = num_samples
		self._bootstrap = bootstrap
		self._bagging = bagging
		self._bagging_instances = None
		self._density_biased = density_biased
		self._db_type = db_type
		self._tuning_factor = tuning_factor
		self._tune_offline = tune_offline
		
		if self._db_type == "KDE":
			self._density_estimator = kde.KDE()
		elif self._db_type == "LSHCount":
			self._density_estimator = lsh_count.LSHCount()


class VSSampling(Sampling):
	
	def __init__(self, num_samples, lower_bound=64, upper_bound=1024, vs_type="EXP", bootstrap=False, bagging=None, density_biased=False, db_type="KDE", tuning_factor=0, tune_offline=False):
		Sampling.__init__(self, num_samples, bootstrap, bagging, density_biased, db_type, tuning_factor, tune_offline)
		self._lower_bound = lower_bound
		self._upper_bound = upper_bound
		self._vs_type = vs_type

	
	def fit(self, data):
		if data is None:
			return
		if self._bagging is not None:				# (not used in the current code(?))
			self._bagging_instances = []
			for i in range(self._num_samples):
				self._bagging.fit(data)
				self._bagging_instances.append(cp.deepcopy(self._bagging))
	
	
	def tune_offline(self, data):			# Pre-tune density estimation parameters (i.e. bandwidth for KDE, or W and rows for LSHCount)
		if not self._density_biased:
			return
		
		if self._db_type == "KDE" or self._db_type == "LSHCount":
			self._density_estimator.tune_parameters(data)

	
	def draw_samples(self, data):
		data_size = len(data)
		
		sampled_indices = []
		if self._bootstrap:					# generate data entries indicies to be sampled (unused in the current version of the code)
			for i in range(self._num_samples):		# ...(with replacement / can have duplicates)
				indices = []
				if self._vs_type == 'FIX':
					indices = np.random.randint(0, data_size, self._lower_bound)
				elif self._vs_type == 'UNI':
					indices = np.random.randint(0, data_size, int(round(np.random.uniform(self._lower_bound, self._upper_bound))))
				elif self._vs_type == 'EXP':
					indices = np.random.randint(0, data_size, int(round(np.power(2.0, np.random.uniform(np.log2(self._lower_bound), np.log2(self._upper_bound))))))
					
				indices.sort()
				sampled_indices.append(indices)
		else:								# (this section runs if using default settings)
			if self._density_biased:		# Estimate densities, for biased sampling
				data_densities = self._evaluate_densities(data)
				if PRINT_STATS:
					if True: print "\ndata densities:\n", data_densities
			
			for i in range(self._num_samples):
				# Compute sample size
				sample_size = 0
				if self._vs_type == 'FIX':
					# Fixed sample size of 64, or n, whichever is lower, i.e. min(n, 64)
					sample_size = min(data_size, self._lower_bound)
				if self._vs_type == 'UNI':				# (any reason for this not being elif? - intentional prioritising?)
					# Uniform distribution from 64 to 1024, i.e. min(n, U(64, 1024))
					sample_size = min(data_size, int(round(np.random.uniform(self._lower_bound, self._upper_bound))))
				elif self._vs_type == 'EXP':			# (EXP is the default)
					# Exponential distribution of 64 <- 2^(6 to 10) <= 1024, i.e. min(n, 2^U(6,10))
					sample_size = min(data_size, int(round(np.power(2.0, np.random.uniform(np.log2(self._lower_bound), np.log2(self._upper_bound))))))
				elif self._vs_type == 'EXP2':			# Moved placement of min
					sample_size = int(round(np.power(2.0, np.random.uniform( min(np.log2(data_size), np.log2(self._lower_bound)), min(np.log2(data_size), np.log2(self._upper_bound) )))))
				elif self._vs_type == 'FIX2':			# Fixed sampling rate
					sample_size = int(round(data_size*0.8))
				
				if not self._density_biased:
					# Uniform sampling
					remain_indices = range(data_size)	# keeps track of the list of indicies not sampled
					new_indices = []
					for j in range(sample_size):		# uniformly choose random points from the dataset (without replacement) until sample size is reached
						index_ind = np.random.randint(0, len(remain_indices), 1)[0]
						index_val = remain_indices[index_ind]
						new_indices.append(index_val)
						remain_indices.remove(index_val)
					new_indices.sort()					# (sequential access may allow for quicker retrieval? - caching)
					sampled_indices.append(new_indices)
				else:
					# Density biased sampling
					new_indices = []
					sample_probabilities = self._convert_to_probabilities(data_densities, sample_size)
					for j in range(data_size):
						if sample_probabilities[j] > np.random.random():
							new_indices.append(j)
					new_indices.sort()					# (sequential access may allow for quicker retrieval? - caching)
					sampled_indices.append(new_indices)
					
					if len(new_indices) < 0.75*sample_size and SAMPLE_SIZE_WARNING:
						print "-- Sample size too low ("+str(len(new_indices))+"/"+str(sample_size)+") --"
					
					if len(new_indices) <= 1:
						print "\nSample size <= 1!"
						print "Magnitude of v="+str(self._tuning_factor)+" is too large.\n"
						# raise ValueError("Magnitude of v="+str(self._tuning_factor)+" is too large.")
					
					if PRINT_STATS:
						if False: print "\ndata densities:\n", data_densities
						if False: print "\nsampling probabilities:\n", sample_probabilities
						print "sample size =", sample_size, " data size =", data_size, " avg prob =", sample_size*1.0/data_size
						print "actual sample size =", len(new_indices)
						print "( Biased:", self._density_biased, " v =", self._tuning_factor, ")\n"
		
		sampled_datas = []
		for i in range(self._num_samples):			# for each sample, create the actual list of data from the entry indicies
			transformed_data = data
			if self._bagging is not None:
				transformed_data = self._bagging_instances[i].get_transformed_data(data)
			sampled_data = []
			for j in sampled_indices[i]:
				sampled_data.append(transformed_data[j])
			sampled_datas.append(sampled_data)
		
		if PRINT_STATS:
			if False: print "sampled data", sampled_datas
		
		return sampled_datas
	
	
	def _evaluate_densities(self, data):
		# Data passed into the density estimators is expected to be without indices
		data = data[:, 1:]
		
		if self._db_type == "KDE":
			self._density_estimator.fit(data, tune_params = not self._tune_offline)
			densities = self._density_estimator.get_densities(data)
		elif self._db_type == "LSHCount":
			self._density_estimator.fit(data, tune_params = not self._tune_offline)
			densities = self._density_estimator.get_counts(data)
		elif self._db_type == "LSHiForest":
			clf = lsh_forest.LSHForest(NUM_ENS, VSSampling(NUM_ENS, density_biased=False), lsh.E2LSH(norm=2))		# Uses L2SH by default
			clf.fit(data)
			densities = clf.decision_function(data).ravel()
			
			# Scale density values from [-1, 0) to [0, 1), as negative values don't work for the calculation of sampling probabilities
			densities = densities + 1
		else:
			raise ValueError("Invalid setting ('" + str(self._db_type) + "') used for DB_TYPE.")
		
		return densities
	
	
	def _convert_to_probabilities(self, densities, sample_size):
		densities = np.power(densities*1.0, self._tuning_factor)
		density_sum = sum(densities)
		sample_probabilities = densities*sample_size / density_sum
		return sample_probabilities
	
	
	def set_tuning_factor(self, v):			# set the tuning factor, v, for density biased sampling
		self._tuning_factor = v
	