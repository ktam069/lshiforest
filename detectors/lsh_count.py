#! /usr/bin/python

import numpy as np
import copy as cp

from lsh import E2LSH

import time

SHOW_PRINTS = False		# Show prints for tuning rows and width	(default: False)

class LSHCount():
	''' Class for density estimation using lsh counts, for biased sampling'''
	
	def __init__(self, bands=100, epsilon=0.1, k=5):
		self._lsh = None
		self._bands = bands
		self._rows = -1
		self._width = -1
		self._epsilon = epsilon
		self._k = k
		self._lsh_instances = None
		self._hash_tables = None
		self._hash_counts = None


	def fit(self, data, tune_params=True):
		indices = range(len(data))
		data = np.c_[indices, data]
		
		if tune_params:
			self._tune_parameters(data)
		
		self._hash_data(data)
	
	
	def _hash_data(self, data):
		if self._rows < 1 or self._width < 0:
			raise ValueError("Attempted to use LSH Count without having tuned parameters.")
		
		self._lsh = E2LSH(bin_width=self._width, norm=2, default_pool_size=self._rows)
		
		self._lsh_instances = []
		for i in range(self._bands):		# for each band, generate a set of LSH functions
			self._lsh.fit(data)
			self._lsh_instances.append(cp.deepcopy(self._lsh))		# store the object (esp. the two arrays), then repeat
		
		self._hash_tables = []				# a list of hash tables (one for each band)
		self._hash_counts = []				# a list of dictionaries of bucket sizes (one for each band)
		for band in range(self._bands):
			hash_table = []					# a list of hashes for each data point (in one band)
			hash_count = {}					# a dictionary of hash counts, {hash : n_i} (for one band)
			
			# print "hash table for band", band, "...",
			# t = time.time()
			
			# Compute the hash and calculate the number of points in each hash bucket
			for i in range(len(data)):
				lsh = self._lsh_instances[band]
				
				x = np.array(data[i][1:])
				a = lsh.A_array
				b = lsh.B_array
				w = lsh._bin_width
				floor = np.floor
				dot = np.dot				
				
				hash_values = [str( int(floor( (dot(x,a[r])+b[r])/w) ) ) for r in range(self._rows)]
				hash = "".join(hash_values)		# all alpha hashes are concatinated to form a single hash
				
				hash_table.append(hash)		# record the hash for this data point (in this band)
				
				try:						# add one to the number of points in the bucket
					hash_count[hash] = hash_count[hash] + 1
				except KeyError:
					hash_count[hash] = 1
			
			# print time.time()-t
			
			self._hash_tables.append(hash_table)
			self._hash_counts.append(hash_count)


	def get_counts(self, data):
		if self._hash_tables is None:		# LSHCount has not been fitted
			return None
		if len(self._hash_tables[0]) < len(data):
			return None
		
		indices = range(len(data))
		data = np.c_[indices, data]
		
		counts = [0]*len(data)				# the lsh count for each data point
		
		for band in range(self._bands):
			# Match data points to the bucket sizes
			for i in range(len(data)):
				hash = self._hash_tables[band][i]
				hash_count = self._hash_counts[band]
				
				counts[i] += hash_count[hash]
			
		counts = np.array(counts)
		counts = counts*1.0 / self._bands
		
		return counts
	
	
	def tune_parameters(self, data):
		indices = range(len(data))
		data = np.c_[indices, data]
		
		self._tune_parameters(data)
	
	
	def _tune_parameters(self, data):
		self._rows = 1
		self._width = 1.0
		
		quantile = int(round(self._epsilon*len(data)))
		
		# Exponentially increase the number of range to find the correct range
		while True:
			self._hash_data(data)
			densities = self.get_counts(data)
			densities.sort()
			
			if SHOW_PRINTS: print "  number of rows:", self._rows
			
			if densities[quantile] < self._k:
				break
			
			self._rows *= 2
		
		# Use binary search to find the exact number of rows
		if self._rows > 1:
			upper_boundary = self._rows
			lower_boundary = self._rows/2
			while upper_boundary > lower_boundary:
				self._rows = int(np.ceil((upper_boundary+lower_boundary)/2.0))
				if self._rows == upper_boundary:
					break
				
				self._hash_data(data)
				densities = self.get_counts(data)
				
				if SHOW_PRINTS: print "  number of rows:", self._rows
				
				diff = self._tail_test(densities)
				if abs(diff) < 0.005:
					break
				elif diff < 0:
					lower_boundary = self._rows
				else:
					upper_boundary = self._rows
		
		# Bound the maximum number of rows
		decrease_width = False
		if self._rows > 100:
			self._rows = 100
			print "Row upper limit of 100 reached."
			decrease_width = True
		
		# Find the width exponentially
		diff = self._tail_test(densities)
		if abs(diff) >= 0.005:
			if decrease_width:
				while True:
					self._width /= 2.0
					
					self._hash_data(data)
					densities = self.get_counts(data)
					densities.sort()
					
					if SHOW_PRINTS: print "  width:", self._width
					
					if densities[quantile] < self._k:
						break
			else:
				while True:
					self._width *= 2.0
					
					self._hash_data(data)
					densities = self.get_counts(data)
					densities.sort()
					
					if SHOW_PRINTS: print "  width:", self._width
					
					if densities[quantile] > self._k:
						break
		
		# Use binary search to find the width
		diff = self._tail_test(densities)
		if abs(diff) >= 0.005:
			if decrease_width:
				lower_width = self._width
				upper_width = self._width*2.0
			else:
				lower_width = self._width/2.0
				upper_width = self._width
			
			if SHOW_PRINTS: print "Searching in window ["+str(lower_width)+", "+str(upper_width)+"]"
			
			while lower_width <= upper_width:
				self._width = (lower_width+upper_width)/2.0
				
				self._hash_data(data)
				densities = self.get_counts(data)
				
				if SHOW_PRINTS: print "  width:", self._width
				
				diff = self._tail_test(densities)
				if abs(diff) < 0.005:
					break
				elif diff < 0:
					upper_width = self._width
				else:
					lower_width = self._width
		
		if SHOW_PRINTS: print "Tuning completed - No. rows:", self._rows, " width:",self._width
	
	
	def _tail_test(self, densities):
		count = 0
		for i in range(len(densities)):
			if densities[i] < self._k:
				count += 1
		
		percentile = count*1.0/len(densities)
		diff = percentile - self._epsilon
		
		return diff
	