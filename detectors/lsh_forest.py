#! /usr/bin/python
#
# Implemented by Xuyun Zhang (email: xuyun.zhang@auckland.ac.nz). Copyright reserved.
#

import numpy as np
import copy as cp

import lsh_tree as lt
import lsh
import klsh
import sampling as sp
import bagging as bg

class LSHForest:
	
	def __init__(self, num_trees, sampler, lsh_family, granularity=1, mode="mean"):
		self._num_trees = num_trees
		self._sampler = sampler
		self._lsh_family = lsh_family
		self._granularity = granularity
		self._trees = []
		self._mode = mode

	
	def display(self):
		for t in self._trees:
			t.display()
	
	
	def tune_offline(self, data):			# Pre-tune density estimation parameters (i.e. bandwidth for KDE, or W and rows for LSHCount)
		self._sampler.tune_offline(data)


	def fit(self, data):
		self.build(data)


	def build(self, data):
		indices = range(len(data))			# create a list with indicies from 0 to n_samples-1
		# Uncomment the following code for continuous values
		data = np.c_[indices, data]			# merge the row index with the attributes of each row
											# i.e. [ [a1, a2, ..., an], [b1, ...], ... ] -> [ [0, a1, a2, ..., an], [1, b1, ...], ... ]

		# Important: clean the tree array
		self._trees = []

		# Sampling data
		self._sampler.fit(data)				# (doesn't do anything with the current/default settings)
		sampled_datas = self._sampler.draw_samples(data)			# returns a list of 'num_ensemblers' samples (each with varying sizes)
		
		# Build LSH instances based on the given data
		lsh_instances = []
		for i in range(self._num_trees):	# for each sample/ensembler, generate a set of values for LSH / LSH functions(?)
			transformed_data = data
			if self._sampler._bagging is not None:
				transformed_data = self._sampler._bagging_instances[i].get_transformed_data(data)	
			self._lsh_family.fit(transformed_data)					# generate two arrays of values from probability distributions
			lsh_instances.append(cp.deepcopy(self._lsh_family))		# store the object (esp. two arrays, hence deepcopy) and continue for loop

		# Build LSH trees
		for i in range(self._num_trees):	# build an LSH tree for each sample
			sampled_data = sampled_datas[i]
			tree = lt.LSHTree(lsh_instances[i])
			tree.build(sampled_data)		# build the tree recursively using the corresponding set of LSH functions
			self._trees.append(tree)

	
	def decision_function(self, data):
		indices = range(len(data))
		# Uncomment the following code for continuous data
		data = np.c_[indices, data]

		depths = []					# a list of lists of anomaly scores s_i
		data_size = len(data)
		for i in range(data_size):			# for each data point x = data[i]
			d_depths = []
			for j in range(self._num_trees):	# for each tree, calculate s_i for the tree and store scores in an array
				transformed_data = data[i]
				if self._sampler._bagging is not None:
					transformed_data = self._sampler._bagging_instances[j].get_transformed_data(np.mat(data[i])).A1
				d_depths.append(self._trees[j].predict(self._granularity, transformed_data))		# value returned is in (0,1]
			depths.append(d_depths)			# adds the list of scores for this point x to the list 'depths'
		
		combined_depths = []				# a list of anomaly scores s
		
		if self._mode=="mean":
			# Arithmatic mean
			for i in range(data_size):			# for each point, calculate the overall score by averaging each list
				depth_avg = 0.0
				for j in range(self._num_trees):
					depth_avg += depths[i][j]
				depth_avg /= self._num_trees
				combined_depths.append(depth_avg)
		elif self._mode=="max":
			# Maximum
			for i in range(data_size):			# for each point, calculate the overall score by averaging each list
				depth_max = 0
				for j in range(self._num_trees):
					depth_max += max(depth_max, depths[i][j])
				combined_depths.append(depth_max)
		elif self._mode=="aom":
			# Average of Maximum (AOM)
			q = 5						# bucket size
			for i in range(data_size):			# for each point, calculate the overall score by averaging each list
				depth_max = [0]*(data_size/q)					# array size = data_size / q	(e.g. 100 / 5 = 20)
				for j in range(self._num_trees):
					depth_max[j/q] += max(depth_max[j/q], depths[i][j])
				aom = float(sum(depth_max)) / len(depth_max)	# take the average of the array
				combined_depths.append(aom)
		else:
			raise ValueError("Invalid setting ('" + str(self._mode) + "') used for averaging scores.")
		
		combined_depths = np.array(combined_depths)
		return -1.0*combined_depths				# change s in (0,1] to s in [-1,0) - for consistency with sklearn outputs
	
	
	def get_avg_branch_factor(self):
		sum = 0.0
		for t in self._trees:
			sum += t.get_avg_branch_factor()
		return sum/self._num_trees
	
	
	def set_mode(self, mode):			# set the combination mode to "mean" (default), "max" or "aom"
		self._mode = mode
	
	
	def tune_parameters(self, data):
		return
