#! /usr/bin/python

import numpy as np
from sklearn.neighbors.kde import KernelDensity
from sklearn.model_selection import GridSearchCV

PRINT_BW = True			# Print tuned bandwidth		(default: False)

class KDE():
	''' Class for density estimation with Kernel Density Estimation'''
	
	def __init__(self, kernel="gaussian", bandwidth=-1):
		self._kernel = kernel
		self._bandwidth = bandwidth
		self._kde = None
	
	
	def fit(self, data, tune_params=True):
		if tune_params:
			self.tune_parameters(data)
		
		kernel = KernelDensity(kernel=self._kernel, bandwidth=self._bandwidth)
		self._kde = kernel.fit(data)
	
	
	def get_densities(self, data):
		if self._kde is None:
			return None
		
		densities = self._kde.score_samples(data)
		densities = np.exp(densities)
		
		# Prevent density values from being overly small (which can generate NaN's with negative values of v)
		densities = densities / np.amax(densities)
		
		return densities
	
	
	def tune_parameters(self, data):
		# Setting 'return_train_score' to False can increase the speed significantly
		grid = GridSearchCV(KernelDensity(kernel="gaussian"),
							{"bandwidth": np.linspace(0.1, 2.0, 20)},
							cv=10,
							return_train_score=False)
		grid.fit(data)
		self._bandwidth = grid.best_params_["bandwidth"]
		
		if PRINT_BW: print "bandwidth (KDE):", self._bandwidth
	