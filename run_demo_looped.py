#! /usr/bin/python
#
# Implemented by Xuyun Zhang (email: xuyun.zhang@auckland.ac.nz). Copyright reserved.
#

import numpy as np
import pandas as pd
import csv
import time
from sklearn.metrics import roc_auc_score
from sklearn.ensemble import IsolationForest

from detectors import VSSampling
from detectors import Bagging
from detectors import LSHForest
from detectors import E2LSH, KernelLSH, AngleLSH


# Run tests in an automated loop, while keeping track of the results

DISABLE_WARNINGS = True		# (May be helpful for ease of viewing - alternatively run script with -W ignore)

PRINT_DETAILS = False		# Print the result of each test 	(default: False)
HIDE_PRINTS = False			# Only display final results		(default: False)
ENABLE_DB = True			# Use density biased sampling instead of uniform	(default: True)
DB_TYPE = "KDE"		# Density estimation method used	(KDE / LSHCount / LSHiForest)
TUNE_OFFLINE = True			# Do parameter tuning offline 		(default: False)
SKIP_LARGE_FILES = True		# Skip over large data sets 		(default: False)

NUM_TESTS = 10				# Number of times to test each classifier	(default: 10)
DATA_START_I = 1			# Data set number to start from				(default: 1)
DATA_END_I 	 = 20			# Data set number to test up to				(default: 20)
DB_FACTOR = 1				# Tuning factor v for density biased sampling (0: uniform sampling)

num_ensemblers = 100		# Number of ensemble components		(default: 100)
rng = np.random.RandomState(42)

sampler = VSSampling(num_ensemblers, density_biased=ENABLE_DB, db_type=DB_TYPE, tuning_factor=DB_FACTOR, tune_offline=TUNE_OFFLINE)

classifiers = [	("sklearn.ISO", IsolationForest(random_state=rng, contamination=0.1)), 
				("ALSH", LSHForest(num_ensemblers, sampler, AngleLSH())), 
				("L1SH", LSHForest(num_ensemblers, sampler, E2LSH(norm=1))), 
				("L2SH", LSHForest(num_ensemblers, sampler, E2LSH(norm=2))), 
				("KLSH", LSHForest(num_ensemblers, sampler, KernelLSH()))]


import warnings
import os.path
import copy as cp

from collections import OrderedDict
table_of_results = OrderedDict()			# Records the AUC values over successive runs for ease of viewing
tables_of_results = []						# Records the AUC values over different values of v
table_of_errors = OrderedDict()				# Records the standard errors
tables_of_errors = []						# Records the standard errors over different values of v
table_of_times = OrderedDict()				# Records the running time of a detector
tables_of_times = []						# Records the running time over different values of v


def run_test_mode():
	global DB_FACTOR
	# test_range = [DB_FACTOR]
	# test_range = np.array(range(-10, 11, 1))*0.1
	# test_range = np.array(range(-1, 2, 1))
	# test_range = np.array(range(-3, 4, 1))*2
	test_range = np.array(range(-3, 4, 1))*1
	
	print "\nSettings:\n"
	print "\tDensity biased sampling: "+str(ENABLE_DB)+", method = "+DB_TYPE+", offline tuning = "+str(TUNE_OFFLINE)
	print "\tRange of v values:", test_range, "\n\tData sets:", DATA_START_I, "-", DATA_END_I, "\n\n____________"
	
	for v in test_range:
		DB_FACTOR = v
		for i in range(1, len(classifiers)):
			classifiers[i][1]._sampler.set_tuning_factor(DB_FACTOR)
		main()
	
	print "\n____________"
	print "\nRange of v values:", test_range
	print_all_statistics()


def main():
	if DISABLE_WARNINGS:	# Disable the display of warnings (which we expect there to be)
		warnings.filterwarnings("ignore")
	
	# Run tests on the specified range of datasets (in "dat/all_data")
	for i in range(DATA_START_I, DATA_END_I+1):
		table_of_results[str(i)] = []
		table_of_errors[str(i)] = []
		table_of_times[str(i)] = []
		
		if i == 13 and SKIP_LARGE_FILES:
			continue
		
		fname = "dat/all_data/"+str(i)+".csv"		# (Files should be named with a number (1-20) only)
		if not os.path.isfile(fname):
			continue
		
		data = pd.read_csv(fname, header=None)
		X = data.values[:, :-1].tolist()
		ground_truth = data.values[:, -1].tolist()
		
		if not HIDE_PRINTS: print "\nData set:", fname, "\n"
		
		run_tests(X, ground_truth, str(i))
		
		if not HIDE_PRINTS: print "\n____________"
	
	tables_of_results.append(cp.copy(table_of_results))
	tables_of_errors.append(cp.copy(table_of_errors))
	tables_of_times.append(cp.copy(table_of_times))
	
	print_statistics()


def run_tests(X, ground_truth, fname):
	if not HIDE_PRINTS: print "\t(Running", NUM_TESTS, "tests for each classifier)"
	
	if TUNE_OFFLINE:
		tune_start_time = time.time()
		sampler.tune_offline(X)
		tune_time = time.time()-tune_start_time
		if not HIDE_PRINTS and TUNE_OFFLINE: print "Offline tuning time:	", tune_time
	
	for i in range(len(classifiers)-1):			# (Use len(classifiers)-1 to skip the last kernel clf)
		(clf_name, clf) = classifiers[i]
		test_classifier(X, ground_truth, clf_name, clf, fname)


def test_classifier(X, ground_truth, clf_name, clf, fname):
	average_auc = 0
	average_train_time = 0
	average_test_time = 0
	
	auc_results = []

	if not HIDE_PRINTS or PRINT_DETAILS: print "\n\t"+clf_name+":"
	if not HIDE_PRINTS and not PRINT_DETAILS: print "\tRunning tests..."

	for i in range(NUM_TESTS):
		if PRINT_DETAILS: print "\t- Test", i+1, "-"
		
		start_time = time.time()
		clf.fit(X)
		train_time = time.time()-start_time
		y_pred = clf.decision_function(X).ravel()
		test_time = time.time()-start_time-train_time
		auc = roc_auc_score(ground_truth, -1.0*y_pred)
		
		if PRINT_DETAILS: print "AUC:	", auc
		if PRINT_DETAILS: print "Training time:	", train_time
		if PRINT_DETAILS: print "Testing time:	", test_time
		
		average_auc += auc
		average_train_time += train_time
		average_test_time += test_time
		auc_results.append(auc)
	
	average_auc /= NUM_TESTS
	average_train_time /= NUM_TESTS
	average_test_time /= NUM_TESTS
	
	table_of_results[str(fname)].append(average_auc)
	sem = compute_standard_error(auc_results)
	table_of_errors[str(fname)].append(sem)
	running_time = average_test_time+average_train_time
	table_of_times[str(fname)].append(running_time)
	
	if PRINT_DETAILS: print ""
	if not HIDE_PRINTS: print "Average AUC:	", average_auc
	if not HIDE_PRINTS: print "Average training time:	", average_train_time
	if not HIDE_PRINTS: print "Average testing time:	", average_test_time


def compute_standard_error(results):
	if len(results) <= 1:
		return 0
	
	mean = sum(results)*1.0 / len(results)
	
	variance = 0
	for i in range(len(results)):
		variance += (results[i] - mean)**2
	variance = variance*1.0 / (len(results)-1)
	std = variance**0.5
	standard_error = std / len(results)**0.5
	
	return standard_error


def print_statistics():				# Print a table of AUC statistics
	print "\nAUC Results: (over", NUM_TESTS, "tests)"
	print "(Density biased sampling: "+str(ENABLE_DB)+", v = "+str(DB_FACTOR)+", method = "+DB_TYPE+", offline tuning = "+str(TUNE_OFFLINE)+")\n"
	
	clf_names = np.array(classifiers)[:, 0].tolist()
	headers = "\t\t"
	for i,val in enumerate(clf_names):
		headers += val + "    \t"
	
	print headers[:-4]
	
	for k,v in table_of_results.items():
		out_str = k + ".csv" + "\t\t"
		for i,val in enumerate(v):
			out_str += "%.2f" %(val*100) + "\t\t"
		print out_str


def print_all_statistics():				# Print a table of AUC statistics
	print "\nSummary AUC Results: (averaged over", NUM_TESTS, "tests)"
	print "(Density biased sampling: "+str(ENABLE_DB)+", method = "+DB_TYPE+", offline tuning = "+str(TUNE_OFFLINE)+")\n"
	
	clf_names = np.array(classifiers)[:, 0].tolist()
	headers = "\t\t"
	for i,val in enumerate(clf_names):
		headers += val + "    \t"
	
	print_tables(tables_of_results, headers)
	
	print "\nStandard errors:"
	print_tables(tables_of_errors, headers, dp=3)
	
	print "\nExecution times:"
	print_tables(tables_of_times, headers, multiplier=1)


def print_tables(tables, headers, dp=2, multiplier=100):
	print headers[:-4]
	for k,v in tables[0].items():
		out_str = k + ".csv" + "\t\t"
		for i in range(len(v)):
			for j in range(len(tables)):
				s_format = "%."+str(dp)+"f"
				out_str += s_format %(tables[j][k][i]*multiplier) + "  "
			out_str += "| "
		print out_str


if __name__ == '__main__':
	if False:
		main()
	else:
		run_test_mode()
